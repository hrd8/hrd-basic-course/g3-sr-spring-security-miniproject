package com.ksga.springminiproject002.payload.dto;

import com.ksga.springminiproject002.model.Role;
import com.ksga.springminiproject002.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    private int id;
    private String fullName;
    private String username;
    private String imageUrl;
    private Set<Role> roles = new HashSet<>();

    public UserDto(User user) {
        this.fullName = user.getFullName();
        this.username = user.getUsername();
        this.id = user.getId().intValue();
        this.imageUrl = user.getImageUrl();
        this.roles = user.getRoles();
    }
}

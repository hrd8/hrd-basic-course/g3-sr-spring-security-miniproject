package com.ksga.springminiproject002.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponsePayload {
    private String status;
    private String error;
    private int code;
    private Object payload;
}

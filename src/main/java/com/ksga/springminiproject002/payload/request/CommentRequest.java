package com.ksga.springminiproject002.payload.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class CommentRequest {
    private String content;
    private int postId;
}

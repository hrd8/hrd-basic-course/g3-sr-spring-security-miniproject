package com.ksga.springminiproject002.payload.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostRequest {
    private String caption;
    private String image;
    private int user_id;
}

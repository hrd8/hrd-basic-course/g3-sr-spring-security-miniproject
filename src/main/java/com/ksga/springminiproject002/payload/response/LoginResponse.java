package com.ksga.springminiproject002.payload.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginResponse{
    private Long id;
    private String jwt;
    private String username;
    private Set<String> roles;
}

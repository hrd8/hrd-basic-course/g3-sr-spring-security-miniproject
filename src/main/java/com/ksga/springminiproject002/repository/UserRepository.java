package com.ksga.springminiproject002.repository;

import com.ksga.springminiproject002.model.Role;
import com.ksga.springminiproject002.model.User;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface UserRepository {
    @Select("SELECT users.id as id,fullname,username,password From users WHERE username = #{name}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "fullName", column = "fullname"),
            @Result(property = "username", column = "username"),
            @Result(property = "password", column = "password"),
            @Result(property = "imageUrl", column = "profile"),
            @Result(property = "isClosed", column = "is_closed"),
            @Result(property = "createAt", column = "created_at"),
            @Result(property = "updateAt", column = "update_at"),
            @Result(property = "roles", column = "id", many = @Many(select = "findAllRolesByUserId"))
    })
    Optional<User> findUserByUsername(@Param("name") String username);

    @Select("SELECT r.id,r.name FROM users_roles ur INNER JOIN roles r ON ur.role_id = r.id WHERE ur.user_id = #{userId}")
    Set<Role> findAllRolesByUserId(Long userId);

    @Insert("INSERT INTO users(username,password,fullname,profile,is_closed,created_at,update_at) VALUES(#{username},#{password},#{fullName},#{imageUrl},#{isClosed},#{createAt},#{updateAt})")
    void save(User user);

}

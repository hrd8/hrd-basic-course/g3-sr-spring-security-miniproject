package com.ksga.springminiproject002.repository;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRoleRepository {
    @Insert("INSERT INTO users_roles (user_id,role_id) VALUES (#{id},#{roleId})")
    void save(@Param("id") int id,@Param("roleId") int roleId);
}

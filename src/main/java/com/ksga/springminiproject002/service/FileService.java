package com.ksga.springminiproject002.service;

import com.ksga.springminiproject002.model.FileUpload;

import java.util.List;

public interface FileService {
    public void save(FileUpload file);

    public FileUpload get(int id);

    public  List<FileUpload> findAll();
}

package com.ksga.springminiproject002.service.impl;

import com.ksga.springminiproject002.model.Comments;
import com.ksga.springminiproject002.model.Replies;
import com.ksga.springminiproject002.payload.request.RepliesRequest;
import com.ksga.springminiproject002.repository.CommentsRepository;
import com.ksga.springminiproject002.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceImp implements CommentService {

    @Autowired
    CommentsRepository commentsRepository;

    public void createComment (Comments comments){
        commentsRepository.createComment(comments);
    }

    public void createReplies (Comments comments){
        commentsRepository.createReplies(comments);
    }

    public List<Comments> getCommentByPostId(int id){
      return commentsRepository.getAllComment(id);
    }

    public Comments getRepliesByParentId(int id){
        return commentsRepository.getAllReply(id);
    }

    @Override
    public List<Replies> getRepliedComment(RepliesRequest request) {
        return commentsRepository.getRepliedComment(request);
    }

    @Override
    public List<Replies> getRepliedByCommentId(int id) {
        return commentsRepository.getRepliedByCommentId(id);
    }

    @Override
    public void addReply(Replies reply) {
        commentsRepository.addReply(reply);
    }
}

package com.ksga.springminiproject002.service;

import com.ksga.springminiproject002.model.post.Post;
import com.ksga.springminiproject002.model.post.PostFilter;
import com.ksga.springminiproject002.payload.dto.PostDto;
import com.ksga.springminiproject002.payload.request.PostRequest;
import com.ksga.springminiproject002.utilities.Paging;

import java.util.List;

public interface PostService {
    public List<PostDto> findAll(Paging paging);
    public PostDto insert(PostRequest post);

    public PostDto findOne(int id);

    public Post update(int id, PostRequest post);
    public boolean delete(int id);
    public List<Post> filterPost(PostFilter filter);
}
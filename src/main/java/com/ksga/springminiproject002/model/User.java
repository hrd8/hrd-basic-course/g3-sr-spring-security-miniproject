package com.ksga.springminiproject002.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private Long id;
    private String fullName;
    private String username;
    private String password;
    private Set<Role> roles = new HashSet<>();
    private String imageUrl;
    private Boolean isClosed = false;
    private Date createAt = new Date();
    private Date updateAt = new Date();

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
}

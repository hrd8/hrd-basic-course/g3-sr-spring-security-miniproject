package com.ksga.springminiproject002.model;

public enum ERole {
    ROLE_USER,
    ROLE_ADMIN;
}

package com.ksga.springminiproject002.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Comments {
    private int id;
    private String content;
    private List<Replies> reply;
    private int userId;
    private int parentId;
    private int postId;
    private Date createdAt =  new Date();
    private Date updateAt =  new  Date();
}
